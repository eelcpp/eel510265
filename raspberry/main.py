#!/usr/bin/env python
__author__ = 'severo-jamir'
# This program logs a Raspberry Pi's CPU temperature to a Thingspeak Channel
# To use, get a Thingspeak.com account, set up a channel, and capture the Channel Key at https://thingspeak.com/docs/tutorials/ 
# Then paste your channel ID in the code for the value of "key" below.
# Then run as sudo python pitemp.py (access to the CPU temp requires sudo access)
# You can see my channel at https://thingspeak.com/channels/41518

# Import libs
import httplib, urllib
import time
import smbus
from sys import argv
import traceback

def sendToThingSpeak(key, value, field, message = ""):
    params = urllib.urlencode({'field'+str(field): value, 'key':key }) 
    headers = {"Content-typZZe": "application/x-www-form-urlencoded","Accept": "text/plain"}
    conn = httplib.HTTPConnection("api.thingspeak.com:80")
    try:
        conn.request("POST", "/update", params, headers)
        response = conn.getresponse()
        print message, time.strftime('%H:%M:%S'), value, field, response.status, response.reason
        data = response.read()
        conn.close()
    except:
        print "connection failed"

def logProcessorTemp():
    key = 'E4ZRRN6Q6P07UFDK'  # Thingspeak channel to update
    temp = int(open('/sys/class/thermal/thermal_zone0/temp').read()) / 1e3 # Get Raspberry Pi CPU temp
    sendToThingSpeak(key, temp, 2, 'Logging processor temp:')

def logExternalTemp():
    key = 'E4ZRRN6Q6P07UFDK'  # Thingspeak channel to update
    temp = 0.4885*getExternalTemp() # Get Raspberry Pi CPU temp
    sendToThingSpeak(key, temp, 1, 'Logging external temp:')
    
def arduinoWriteNumber(value, bus):
    bus.write_byte(0x04, value)
    # bus.write_byte_data(address, 0, value)
    return -1

def arduinoReadNumber(bus):
    number = bus.read_byte(0x04)
    # number = bus.read_byte_data(address, 1)
    return number

def getExternalTemp():
        # for RPI version 1, use "bus = smbus.SMBus(0)"
        bus = smbus.SMBus(1)
                       
        arduinoWriteNumber(1, bus)
        time.sleep(1)
        
        number = arduinoReadNumber(bus)
        
        return int(number)

if __name__ == "__main__":     #Report Raspberry Pi internal temperature to Thingspeak Channel
   
    if argv[1] == "--processor":
        try:
            if argv[2] == "--loop":
                while True:
                    logProcessorTemp()
                    time.sleep(1)
            elif argv[2].startswith('--finite'):
                for i in range(int(argv[3])):
                    logProcessorTemp()
                    time.sleep(1)
            else:
                print "Incorrect usage! Try --loop or --finite 3."
        except:
            print "Doing nothing ..."
            traceback.print_exc()
    
    elif argv[1] == "--external":
        try:
            if argv[2] == "--loop":
                while True:
                    logExternalTemp()
                    time.sleep(1)
            elif argv[2].startswith('--finite'):
                for i in range(int(argv[3])):
                    logExternalTemp()
                    time.sleep(1)
            else:
                print "Incorrect usage! Try --loop or --finite 3."
        except:
            print "Doing nothing ..."
            traceback.print_exc()
    else:
        None