// EEL 510265 - 2015.2
// William Silva, Daniel Severo
// Code for Arduino Uno to Read Temperature from sensor LM35

#include <Wire.h>
#define SLAVE_ADDRESS 0x03

void setup() {
    pinMode(13, OUTPUT);
    Serial.begin(9600);
    Wire.begin(SLAVE_ADDRESS);
    Wire.onRequest(dataOut);
    Serial.println(" Ready");
}

void loop() 
  {
    delay(100);
  }

void dataOut(){
  int sensorValue = analogRead(A0);
  float temp = sensorValue * 0.48828125;
  Serial.println(" Debug Info: Value that was sent");
  Serial.println(sensorValue);  
  Wire.write(sensorValue);
}

