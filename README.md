# CONTAINER PROJECT 2015/2 #

## Project Details ##

* **Authors:** Daniel de Souza Severo, William Jamir Silva.
* **Location:** Federal University of Santa Catarina - Department of Electrical & Electronics Engineering.
* **Course:** B.Eng./M.Sc. Electrical Engineering.
* **Date:** Second semester of 2015.

## What is it? ##

Source code in Python/C that reads the temperature from 2 Arduino Uno's through a Raspberry Pi.

## What do I need to run it? ##

#### A computer with: ####
* [Arduino IDE](http://wiki.python.org.br/) installed.

#### A Raspberry Pi (any model) with: ####
* [Raspbian](https://www.raspbian.org/) or other compatible OS.
* [Python](http://wiki.python.org.br/) 2.7.6 or superior.
* [i2cdetect](http://skpang.co.uk/blog/archives/575) (optional for debugging).

#### Arduino Uno (1 or 2) with: ####
* The code provided!

#### Others: ####
* An account on [ThingSpeak](https://thingspeak.com/).
* Miscellaneous cables for connections/power.

## How do I use it? ###

#### Setup ####
* Create a channel on your [ThingSpeak](https://thingspeak.com/) account and generate an API key.
* On the raspberry pi: [enable the I2C interface](https://learn.adafruit.com/adafruits-raspberry-pi-lesson-4-gpio-setup/configuring-i2c).
* On the raspberry pi: [enable ssh](https://www.raspberrypi.org/documentation/remote-access/ssh/).
* On your pc: Download the code to the pi by running the code below in a bash shell or downloading it from the [course website](www.gse.ufsc.br): 
```
#!bash
git clone https://DSevero@bitbucket.org/eelcpp/eel510265.git

```
* On your pc: send the python directory to the raspberry pi.
* On your pc: compile and upload the arduino folder to Arduino Uno.
* On the raspberry pi: change the "E4ZRRN6Q6P07UFDK" key to your channel key.
* Connect the cables like below:

![SensorProjectSketch.png](https://bitbucket.org/repo/gpe5GX/images/1906214716-SensorProjectSketch.png)

#### Using the code: ####
The only file that is used is python/main.py. Enable execution of the python/main.py by running the following in a bash shell:

```
#!bash

sudo chmod +x python/main.py

```
The code supports the following options:
```
#!bash
--external
```
```
#!bash
--processor
```
The --external tells the script to get the temperature from the Arduino's, while --processor will make it log the internal processor temperature.
```
#!bash
--loop
```
```
#!bash
--finite X
```

X can be any number. The first options (--loop) tells the script to get the temperature every 1 second, forever. The second option (--finite) will tell it to get the temperature every 1 second, but only X times.

## Who do I talk to? ##
* Daniel de Souza Severo - danielsouzasevero@gmail.com
* William Jamir Silva - williamjamir@gmail.com